#!/usr/bin/env python
# coding: utf-8

# In[2]:


get_ipython().run_cell_magic('capture', '', '!pip install stanza\n!pip install nltk\n')


# In[3]:


import requests
import json
import pandas as pd
import numpy as np
from os import makedirs, listdir, getenv
from os.path import join, exists
from datetime import date, timedelta
from time import sleep
from IPython.display import clear_output
import pickle
import stanza
#stanza.download('en')
import nltk
from nltk.collocations import *
from tqdm import tqdm
from sys import exit


# In[4]:


# CREATE folders and paths
datasets_path = join('.', 'datasets')
makedirs(datasets_path, exist_ok=True)

# create sub directory for new york times if not exists
nyt_path = join(datasets_path, "nyt")
makedirs(nyt_path, exist_ok=True)

# create sub directory for the guardian if not exists
guardian_path = join(datasets_path, "guardian")
makedirs(guardian_path, exist_ok=True)

listdir(datasets_path)


# # Guardian DATA
# 
# ### Download by daterange

# In[69]:


# Edit range to change the data
start_date = date(2023, 8, 1)
end_date = date(2023, 8, 2)

dayrange = range((end_date - start_date).days + 1)
d_range_str = f"{start_date}--{end_date}"  # string to name the pickle data files

# Create a list of json file names according to the date-range
json_file_list = []

# Source for the synonyms: https://www.powerthesaurus.org/homosexual/synonyms
word_list = ["homosexual", "gay", "homo", "queer", "homoerotic", "lesbian", 
             "faggot", "lgbt", "lgbtq", "same-sex"]

MY_API_KEY = os.getenv('MY_API_KEY')  # Guardian API key

# The code here is partly taken from: https://gist.github.com/dannguyen/c9cb220093ee4c12b840
API_ENDPOINT = f'https://content.guardianapis.com/search?q={"%20OR%20".join(word_list)}'
my_params = {
    'from-date': "",
    'to-date': "",
    'order-by': "newest",
    'show-fields': 'all',
    'page-size': 100,
    'api-key': MY_API_KEY
}

for daycount in dayrange:
    dt = start_date + timedelta(days=daycount)
    datestr = dt.strftime('%Y-%m-%d')
    fname = join(guardian_path, datestr + '.json')
    if not exists(fname):  # Then let's download it
        sleep(0.5)  # To not overwhealm the Guardian API wait for a bit
        clear_output(wait=True)  # Clears the print-output
        print("Downloading", datestr)
        all_results = []
        my_params['from-date'] = datestr
        my_params['to-date'] = datestr
        current_page = 1
        total_pages = 1
        while current_page <= total_pages:
            print("...page", current_page)
            my_params['page'] = current_page
            resp = requests.get(API_ENDPOINT, my_params)
            data = resp.json()
            all_results.extend(data['response']['results'])
            # if there is more than one page
            current_page += 1
            total_pages = data['response']['pages']

        with open(fname, 'w') as f:
            print("Writing to", fname)

            # re-serialize it for pretty indentation
            f.write(json.dumps(all_results, indent=2))
    # Add the file name to our json-list
    if exists(fname):
        json_file_list.append(fname)

print("Date range", d_range_str, "done.\n")


# ### Filter again and create DF

# In[70]:


# Data
data_dict = {"date": [],
             "headline": [],
             "text": []}

# Transform the raw data from json files into pandas data frame

empty_files = 0

for fname in json_file_list:
    date_str = fname[-15:-5]
    okay = False
    with open(fname, 'r') as f:
        # returns JSON object as a dictionary
        raw_data = json.load(f)
        if not len(raw_data) > 0:
            # File is empty..
            empty_files += 1
        for entry in raw_data:
            # Get headline
            headline = entry["fields"]["headline"]
            #standfirst = entry["fields"]["standfirst"]
            # Get the body-text
            body_text = entry["fields"]["bodyText"]
            #print("len(body_text):", len(body_text))
            # Add to dictionary
            data_dict["date"].append(date_str)
            data_dict["headline"].append(headline)
            data_dict["text"].append(body_text)

if empty_files > 5:
    print("\nWARNING:", empty_files, "out of", 
          len(json_file_list), "files are empty.")

# Build the pandas DataFrame
guardian_df = pd.DataFrame(data_dict)  
guardian_df      



# In[71]:


if len(guardian_df.index) < 6:
    for i in guardian_df.index:
        print(guardian_df["headline"][i], "|", guardian_df["date"][i], "\n", guardian_df["text"][i], "\n\n")
    


# In[72]:


import pickle
# save the df with pickle
with open(f"guardian_df_{d_range_str}.pickle", "wb") as f:
    pickle.dump(guardian_df, f)


# # Data Processing

# ### Tokenization and cleaning

# In[73]:


# load the df with pickle
with open(f"guardian_df_{d_range_str}.pickle", "rb") as f:
    guardian_df = pickle.load(f)
guardian_df.head(2)


# In[74]:


nlp = stanza.Pipeline(lang="en", processors="tokenize, pos, mwt, lemma", 
                      verbose=False, use_gpu=True)

bigram_measures = nltk.collocations.BigramAssocMeasures()

# Clean data
data_dict = {"date": [],
             "headline": [],
             "tokens": []}

total_rows = len(guardian_df)

# Search list to clean data
word_list_lower = [word.lower() for word in word_list]
print("Search words:", word_list_lower)
nr_false_articles = 0

# Check whether the step has been done already 
#     (because takes few mins for a one year batch of articles) 
if f"clean_df_{d_range_str}.pickle" in listdir():
    print("Tokenization has been done already..\n")
#if "tokens" in guardian_df.columns:
#    print("Tokenization has been done already..\n")
else:

    # Create a tqdm progress bar
    with tqdm(total=total_rows) as pbar:
        for i, row in guardian_df.iterrows():
            article = row["text"]
            preprocess = nlp(article)
            tokens = [word.lemma.lower() for sent in preprocess.sentences 
                              for word in sent.words if word.upos in ["NOUN", "ADJ"]]
            # Data Cleaning
            finder = BigramCollocationFinder.from_words(tokens)
            finder.apply_freq_filter(3)  # Occurance > 3 times
            
            # Problem is that the Guardian API sometimes returns articles in which none of our search words is found. 
            # Hence, we need to filter them out..
            if not any(word in word_list_lower for word in tokens):
                nr_false_articles += 1
                pbar.update(1)
                continue

            # Filter tokens (replace bigrams)
            nr_tokens = len(tokens)
            for bigram in finder.nbest(bigram_measures.pmi, -1):
                for i in range(nr_tokens):
                    if bigram[0] == tokens[i] and i+1 < nr_tokens and bigram[1] == tokens[i+1]:
                        # Found the bigram, replace it
                        tokens[i] = f"{bigram[0]}_{bigram[1]}"
                        tokens[i+1] = False

            # Filter out the marked entries (save list)
            tokens = [tok for tok in tokens if not tok == False]
            # Add to data dictionary
            data_dict["date"].append(row["date"])
            data_dict["headline"].append(row["headline"])
            data_dict["tokens"].append(tokens)

            # Update the progress bar
            pbar.update(1)

    clean_df = pd.DataFrame(data_dict)

    # save the df with pickle
    with open(f"clean_df_{d_range_str}.pickle", "wb") as f:
        pickle.dump(clean_df, f)
    print("\nWarning:", nr_false_articles, "articles out of", 
          total_rows, "did not contain any of the search words above.")
    clean_df.head()


# # Cooccurance analysis

# In[75]:


# load the df with pickle
with open(f"clean_df_{d_range_str}.pickle", "rb") as f:
    guardian_df = pickle.load(f)
guardian_df.tail()


# In[76]:


bigram_measures = nltk.collocations.BigramAssocMeasures()
# Score in whole article with window size
# The window_size argument specifies the number of words on either side of a target word 
# that should be considered when identifying collocations
window_size = 10  # The average number of words in an English sentence is in the range of 10 to 20 words
search_levels = 2
reduced_word_list = ["homosexual", "gay", "homo", "lesbian", "faggot", "same-sex"]

bigram_dicts = []
for i in range(search_levels):
    bigram_dicts.append(dict())

# Create a tqdm progress bar
total_rows = len(guardian_df)
with tqdm(total=total_rows) as pbar:
    for r, row in guardian_df.iterrows():
        # Specific words from which the cooccurences are to be searched from
        specific_words = reduced_word_list.copy()
        search_words = specific_words
        # Get tokens
        tokens = row["tokens"]
        # Extraction of bigrams
        finder = BigramCollocationFinder.from_words(tokens, window_size = window_size)
        # Remove bigrams with two identical words
        finder.apply_ngram_filter(lambda w1, w2: w1 == w2)
        # We use the pmi measure
        bg_score_list = finder.score_ngrams(bigram_measures.pmi)
        # Remove bigrams with scores below threshold 
        threshold = 1.5
        bg_score_list = [(bigram, score) for bigram, score in bg_score_list if score >= threshold]
        # Sort the bigrams according to the scores
        sorted_bgs = sorted(bg_score_list, key=lambda x:x[1], reverse=True)
        # Remove bigram pairs with same words
        l = len(sorted_bgs)
        for i in range(l):
            if i+1 < l and sorted_bgs[i][1] == sorted_bgs[i+1][1] and sorted_bgs[i][0][:] == sorted_bgs[i+1][0][::-1]:
                sorted_bgs[i] = False  # Mark the found duplicate
        # Filter out the marked entries
        bg_score_list = [bg for bg in sorted_bgs if not bg == False]
        # Search step by step (search levels)
        for i in range(search_levels):
            # Filter the bigrams
            #print(f"Level {i}: searching for {search_words}")
            if i == 0:  # In the first round we take any bigram
                found_bigrams = [bigram for bigram in bg_score_list if any(word in bigram[0] for word in search_words)]
            else:
                # Add the search_words to the list of words we have already searched for
                specific_words += search_words
                # Here we don't want to find bigrams that contain two words we already searched for
                found_bigrams = [bigram for bigram in bg_score_list if any(word in bigram[0] for word in search_words) and not all(word in specific_words for word in bigram[0])]
            # New search words:
            search_words = []
            for bg in found_bigrams:
                # Sort the two words of the bigrams to have a fixed order
                sorted_bg_tuple = tuple(sorted(bg[0]))
                # Add up the score values
                bigram_dicts[i][sorted_bg_tuple] = bigram_dicts[i].get(sorted_bg_tuple, 0) + bg[1]
                # Append new search words (if we have not searched for them yet)
                search_words += [w for w in sorted_bg_tuple if w not in specific_words]
            # Make sure search_words does not contain duplicates
            search_words = list(set(search_words))
        # Update the progress bar
        pbar.update(1)
        


# In[77]:


# Save the bigram_dicts as pickle files
with open(f"guardian_bigram_dicts_{d_range_str}.pickle", "wb") as f:
        pickle.dump(bigram_dicts, f)


# ### Print the cooccurence graph

# In[78]:


# Load the bigram_dicts with pickle
with open(f"guardian_bigram_dicts_{d_range_str}.pickle", "rb") as f:
    bigram_dicts = pickle.load(f)


# In[79]:


import networkx as nx
import matplotlib.pyplot as plt

cooccurrences = []

nr_of_bgs = 10
for bg_dict in bigram_dicts:
    if len(cooccurrences) == 0:
        cooccurrences = sorted(list(bg_dict.items()), 
                               key=lambda x:x[1], 
                               reverse=True)[:nr_of_bgs]
    else:
        # Take only cooccurrences that match with the ones we already have
        coocc_words = []
        for bg in [coocc[0] for coocc in cooccurrences]:
            for word in bg:
                if word not in coocc_words:
                    coocc_words.append(word)
        new_bgs = [bg for bg in list(bg_dict.items()) if any(word in bg[0] for word in coocc_words)]
        # Sort them
        new_cooccs = sorted(new_bgs, key=lambda x:x[1], reverse=True)
        # Add them
        if len(new_cooccs) < nr_of_bgs -1:
            cooccurrences += new_cooccs
        else:
            cooccurrences += new_cooccs[:nr_of_bgs]

print("cooccurrences", cooccurrences)
            
# Normalize weights (to values between 1 and 10)
nrmlzd_cooccs = []
scores = [bg[1] for bg in cooccurrences]
s_min = min(scores)
s_max = max(scores)
for coocc in cooccurrences:
    nrmlzd_score = int(10*(0.1 + (coocc[1] - s_min) / (s_max - s_min + 1e-8)))
    nrmlzd_cooccs.append((coocc[0], nrmlzd_score))
    
cooccurrences = nrmlzd_cooccs
    
# Empty graph
graph = nx.Graph()

# Add cooccurences into the graph
for cooccurrence in cooccurrences:
    bigram, score = cooccurrence
    w1, w2 = bigram
    graph.add_edge(w1, w2, weight=score)

# Calculate the positions of verticies
pos = nx.spring_layout(graph)

# Extract edges and their weights (the scores)
edges = graph.edges()
weights = [graph[edge[0]][edge[1]]['weight'] for edge in edges]

labels = {node: node for node in graph.nodes()}
# options
options = {"edgecolors": "tab:gray", "node_size": 200, "alpha": 0.8,
           "font_weight": 'bold', "with_labels": True,
           "edge_color": 'tab:gray', "font_weight": 'bold'}
# Draw the graph 
nx.draw(graph, pos, edgelist=edges, width=weights, node_color="tab:red", **options)
# Draw edge labels showing the weight values only
labels = {(edge[0], edge[1]): graph[edge[0]][edge[1]]['weight'] for edge in edges}
nx.draw_networkx_edge_labels(graph, pos, edge_labels=labels, font_color='blue')

# Save the plot as a PDF file
plt.savefig(f"Guardian_graph_{d_range_str}.pdf", format="pdf")
# Show the graph
plt.show()


# ### Paradigm shift evaluation

# In[80]:


# Load the bigram_dicts with pickle
with open(f"guardian_bigram_dicts_{d_range_str}.pickle", "rb") as f:
    bigram_dicts = pickle.load(f)

d_range_str


# In[98]:


# Date ranges to compare bigram dicts on
#date_range_1 = "1957-01-01--1969-12-31"
#date_range_2 = "2018-01-01--2022-12-31"
#date_range_1 = "2023-06-01--2023-06-30"
#date_range_2 = "1957-01-01--1989-12-31"
#date_range_1 = "2023-08-01--2023-08-02"
date_range_2 = "2018-01-01--2022-12-31"
date_range_1 = "2023-06-01--2023-06-30"

n = 200
w_list = reduced_word_list.copy()
print("Topic words are:", w_list)

# Load bigram dicts (if they exist)
def get_normalized_cooccs(file_name, words_list, n=20):
    if file_name in listdir():
        with open(file_name, "rb") as f:
            bg_dict = pickle.load(f)[0]  # Take the first dictionary (level 1)
        cooccurrences = sorted(list(bg_dict.items()), key=lambda x:x[1], reverse=True)
        cooccs_dict = {}  # To be returned
        for word in words_list:
            n_bgs_per_word = [bg for bg in cooccurrences if word in bg[0]][:n]
            if len(n_bgs_per_word) == 0:
                cooccs_dict[word] = []
                print(f"No co-occurrences for '{word}' in {file_name[-29:-7]}")
                continue
            # Remove the word and only leave the cooccurrence
            l_0 = [(bg[0][0], bg[1]) for bg in n_bgs_per_word if word == bg[0][1]]
            l_1 = [(bg[0][1], bg[1]) for bg in n_bgs_per_word if word == bg[0][0]]
            n_bgs_per_word = l_0 + l_1
            
            # Normalize weights (to values between 0 and 1)
            nrmlzd_n_bgs = []
            scores = [bg[1] for bg in n_bgs_per_word]
            s_min = min(scores)
            s_max = max(scores)
            for bg in n_bgs_per_word:
                nrmlzd_score = (bg[1] - s_min) / (s_max - s_min + 1e-8)
                nrmlzd_n_bgs.append((bg[0], nrmlzd_score))
            cooccs_dict[word] = nrmlzd_n_bgs
        return cooccs_dict
    
    else:
        print(f"\nWARNING: The file {file_name} doesn't exist!\n")
        return {}

        
cooccs_1 = get_normalized_cooccs(f"guardian_bigram_dicts_{date_range_1}.pickle", w_list, n)
cooccs_2 = get_normalized_cooccs(f"guardian_bigram_dicts_{date_range_2}.pickle", w_list, n)

if len(cooccs_1.keys()) > 0 and len(cooccs_2.keys()) > 0:
    # Find the overlapping words and the non-overlapping
    g_scores = []  # Scores of overlapping cooccs ("gleich")
    u_scores = []  # Scores of non-overlapping cooccs ("ungleich")
    
    for key in w_list:
        w_dict_1 = {tup[0]: tup[1] for tup in cooccs_1[key]}
        w_dict_2 = {tup[0]: tup[1] for tup in cooccs_2[key]}
        for word in w_dict_1.keys():
            if word in w_dict_2.keys():
                g_scores.append(w_dict_1[word])  # Add the score to g_scores
            else:
                u_scores.append(w_dict_1[word])  # Add it to u_scores
        for word in w_dict_2.keys():  # Same for w_dict_2
            if word in w_dict_1.keys():
                g_scores.append(w_dict_2[word])
            else:
                u_scores.append(w_dict_2[word])
    
    #print("g_scores:", g_scores, "\nu_scores:", u_scores)
    g_sum = sum(g_scores)
    u_sum = sum(u_scores)
    if g_sum < u_sum:
        print(f"\n\nThere is a paradigm shift: w(G_n) < w(U_n) with n={n}",
              f"\nBetween time range {date_range_1} and {date_range_2} with:",
              f"\nw(G_n) = {g_sum:.{3}f}    and",
              f"\nw(U_n) = {u_sum:.{3}f}.")
    else:
        print(f"No paradigm shift found. Values are w(G_n)={g_sum:.{3}f} and w(U_n)={u_sum:.{3}f}")
    



